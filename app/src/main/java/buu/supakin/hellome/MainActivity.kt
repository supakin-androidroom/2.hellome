package buu.supakin.hellome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val LOG = "MAIN ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val editName = findViewById<EditText>(R.id.editName)
        val btnHello = findViewById<Button>(R.id.btnHello)
        val txtHello = findViewById<TextView>(R.id.txtHello)

        btnHello.setOnClickListener({
            var msg = ""
            if (editName.text.toString() == "" ) msg = "Please Input Your Name"
            else msg = "Hello ${editName.text.toString()} !"
            Toast.makeText(this@MainActivity, msg, Toast.LENGTH_LONG).show()
            Log.d(LOG, "Click Me")
            txtHello.text = msg
        })
    }

    override fun onClick(view: View?) {
        Toast.makeText(this, "Click Me", Toast.LENGTH_LONG).show()
    }
}